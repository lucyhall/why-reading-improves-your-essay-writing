# Why Reading Improves Your Essay Writing

George Martin, an American novelist and short-story author and the author of the series of epic fantasy novels A Song of Ice and Fire once said: _“A reader lives a thousand lives before he dies . . . The man who never reads lives only one.”_

It’s no doubt that reading is beneficial for our professional and personal lives. In this article, we’re going to find out why reading improves your essay writing. First off, reading involves two important things, understanding the subject matter and helping the memory absorb what you read thus facilitating the learning process. Multiple studies suggest that [reading](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4063364/) improves writing.

Let’s see why reading improves writing.

### Reading Eliminates Writer’s Block
Writer’s block is real. It’s one of the experiences you never want to experience. Writer’s block implies a time when you feel not inspired or you can’t think of anything to write. It can be dangerous when you have a lot of writing work to do, especially if you have too many assignments to handle.
Reading can help to beat writer’s block since you gain a lot of information so you don’t lack something to write about.  
As a student, you can get inspiration by reading samples from pro essay writers like those shared in this like [https://edubirdie.com/examples/](https://edubirdie.com/examples/). When you read all the essay examples of college students and other expert writers, you can get inspired.

### Reading Broadens Your Vocabulary 
Another reason why reading improves your writing is that it broadens your vocabulary. While reading books, articles, and other publications, keep your dictionary so that you can look up the definitions and meaning of new words. By so doing, you will be building your vocabulary which helps you to discover new words you can use in your essay writing.

### Reading Improves Grammar 
Apart from improving your vocabulary, reading helps to boost your grammar. Grammar is one of the most important components of essay writing. No matter how super your writing is if it’s full of grammatical errors, you aren't going to get high grades on your paper.
Reading will help to increase your grammar skills since you will become familiar with grammar rules so you can easily incorporate them into your essay writing process.

### Helps You to Learn How to Format Your Essay Writing
Different essays have different formats. Reading can help you learn how to format your essay writing. This is because you learn the basic structure rules to apply to any type of writing. You also learn how to organize your points logically while transitioning from one point to another.

### Reading Enables You to Discover Different Writing Styles
You probably know only a few writing styles, right?
Now when you incorporate reading into your daily routine, you will discover a ton of writing styles you will never have thought existed. You will also find that different writers have their writing styles.

### Reading Improves Concentration
Essay writing requires a lot of research, thus it can be hard to concentrate at the start of the writing process. Reading regularly helps to improve your concentration ability since you have to concentrate for you to read and understand a concept. The same level of concentration is needed during [essay writing](https://www.ccny.cuny.edu/sites/default/files/writing/upload/EssayStructure_Basic.pdf).
So there you have it. If you hate reading, it could be the reason why you struggle with essay writing. Start reading regularly and see the benefits you can get.

